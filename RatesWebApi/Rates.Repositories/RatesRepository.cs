﻿using Dapper;
using Rates.Interfaces;
using Rates.Interfaces.connection;
using Rates.Models;
using System.Data;

namespace Rates.Repositories
{
    public class RatesRepository : IRatesRepository
    {
        private readonly IRatesDbConnectionContext _dbConnectionContext;
        private IDbTransaction DbTransaction => _dbConnectionContext.GetTransaction();
        private IDbConnection DbConnection => _dbConnectionContext.GetConnection();
        public RatesRepository(IRatesDbConnectionContext dbConnectionContext)
        {
            _dbConnectionContext = dbConnectionContext;
        }

        public async Task SaveRates(IEnumerable<RatesDTO> rates)
        {
            await DbConnection.ExecuteAsync("spInsertRate", rates.ToArray(), commandType: CommandType.StoredProcedure, transaction: DbTransaction);
        }

        public async Task<IEnumerable<RatesDTO>> GetRates(int offset, int rows)
        {
            var query = @"SELECT *
                    FROM [Rates] ORDER BY [Price]
                    OFFSET @offset ROWS
                    FETCH NEXT @rows ROWS ONLY";

            var rates = await DbConnection.QueryAsync<RatesDTO>(query, new { offset, rows }, transaction: DbTransaction);

            return rates.ToArray();
        }
    }
}
