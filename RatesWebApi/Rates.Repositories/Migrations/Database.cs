﻿using Dapper;
using Microsoft.Data.SqlClient;

namespace Rates.Repositories.Migrations
{
    public static class Database
    {
        public static void EnsureDatabase(string connectionString, string databaseName)
        {
            var parameters = new DynamicParameters();
            parameters.Add("name", databaseName);

            using var connection = new SqlConnection(connectionString);
            var records = connection.Query("SELECT * FROM sys.databases WHERE name = @name", parameters);

            if (!records.Any())
            {
                connection.Execute($"CREATE DATABASE RatesDb");
            }
        }
    }
}
