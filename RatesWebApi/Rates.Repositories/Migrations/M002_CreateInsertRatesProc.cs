﻿using FluentMigrator;

namespace Rates.Repositories.Migrations
{
    [Migration(202202042231)]
    public class M002_CreateInsertRatesProc : Migration
    {
        public override void Down()
        {
            
        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spInsertRate
                @Id int,
                @MarketPairs int,
                @CmcRank int,
                @Name nvarchar(200),
                @Symbol nvarchar(50),
                @Platform nvarchar(200),
                @Slug nvarchar(10),
                @Tags nvarchar(max),
                @DateAdded datetime,
                @LastUpdated datetime,
                @Price decimal(19,5),
                @MarketCap decimal(19,5),
                @MarketCapDominance decimal(19,5),
                @FullyDilutedMarketcap decimal(19,5),
                @Volume24h decimal(19,5),
                @VolumeChange24h decimal(19,5),
                @PercentChange24h decimal(19,5),
                @PercentChange7d decimal(19,5)
                AS
                BEGIN
                INSERT INTO [Rates]
                           ([Id]
                           ,[MarketPairs]
                           ,[CmcRank]
                           ,[Name]
                           ,[Symbol]
                           ,[Platform]
                           ,[Slug]
                           ,[Tags]
                           ,[DateAdded]
                           ,[LastUpdated]
                           ,[Price]
                           ,[MarketCap]
                           ,[MarketCapDominance]
                           ,[FullyDilutedMarketcap]
                           ,[Volume24h]
                           ,[VolumeChange24h]
                           ,[PercentChange24h]
                           ,[PercentChange7d])
                     VALUES
                           (@Id
                           ,@MarketPairs
                           ,@CmcRank
                           ,@Name
                           ,@Symbol
                           ,@Platform
                           ,@Slug
                           ,@Tags
                           ,@DateAdded
                           ,@LastUpdated
                           ,@Price
                           ,@MarketCap
                           ,@MarketCapDominance
                           ,@FullyDilutedMarketcap
                           ,@Volume24h
                           ,@VolumeChange24h
                           ,@PercentChange24h
                           ,@PercentChange7d)
                END
                ");
        }
    }
}
