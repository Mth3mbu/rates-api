﻿using FluentMigrator;

namespace Rates.Repositories.Migrations
{
    [Migration(202202042120)]
    public class M001_CreateRatesTable : Migration
    {
        public override void Down()
        {
            Delete.Table("Rates");
        }

        public override void Up()
        {
            Create.Table("Rates")
             .WithColumn("Id").AsInt32().NotNullable().PrimaryKey()
             .WithColumn("MarketPairs").AsInt32()
             .WithColumn("CmcRank").AsInt32()
             .WithColumn("Name").AsString(200).Nullable()
             .WithColumn("Symbol").AsString(50).Nullable()
             .WithColumn("Platform").AsString(int.MinValue).Nullable()
             .WithColumn("Slug").AsString(10).Nullable()
             .WithColumn("Tags").AsString(int.MaxValue)
             .WithColumn("DateAdded").AsDateTime()
             .WithColumn("LastUpdated").AsDateTime()
             .WithColumn("Price").AsDecimal()
             .WithColumn("MarketCap").AsDecimal()
             .WithColumn("MarketCapDominance").AsDecimal()
             .WithColumn("FullyDilutedMarketcap").AsDecimal()
             .WithColumn("Volume24h").AsDecimal()
             .WithColumn("VolumeChange24h").AsDecimal()
             .WithColumn("PercentChange24h").AsDecimal()
             .WithColumn("PercentChange7d").AsDecimal();
        }
    }
}
