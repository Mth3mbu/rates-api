﻿using AutoMapper;
using Rates.Interfaces;
using Rates.Models;

namespace Rates.Services
{
    public class RatesService : IRatesService
    {
        private readonly ICoinMarketCapService _coinMarketCapService;
        private readonly IRatesRepository _ratesRepository;
        private readonly IMapper _mapper;
        public RatesService(ICoinMarketCapService coinMarketCapService, IRatesRepository ratesRepository, IMapper mapper)
        {
            _coinMarketCapService = coinMarketCapService;
            _ratesRepository = ratesRepository;
            _mapper = mapper;
        }
        public async Task<IEnumerable<Rate>> GetRates(int start = 1, int limit = 10)
        {
            var existingRates = await _ratesRepository.GetRates(start - 1, limit);

            if (existingRates.Count() > 0)
                return existingRates.Select(rate => _mapper.Map<Rate>(rate));

            var rates = await _coinMarketCapService.GetRates(start, limit);

            var ratesList = rates.Select(rate => _mapper.Map<RatesDTO>(rate)).ToList();

            await _ratesRepository.SaveRates(ratesList);

            return rates;
        }
    }
}
