﻿namespace Rates.Models
{
    public class Rate
    {
        public int Id { get; set; }
        public int MarketPairs { get; set; }
        public int CmcRank { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string Platform { get; set; }
        public string Slug { get; set; }
        public string[] Tags { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime LastUpdated { get; set; }
        public decimal Price { get; set; }
        public decimal MarketCap { get; set; }
        public decimal MarketCapDominance { get; set; }
        public decimal FullyDilutedMarketcap { get; set; }
        public decimal Volume24h { get; set; }
        public decimal VolumeChange24h { get; set; }
        public decimal PercentChange24h { get; set; }
        public decimal PercentChange7d { get; set; }
    }
}