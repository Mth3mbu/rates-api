﻿namespace Rates.TestInfrastructure.Builders
{
    public class PaginationTestDataBuilder
    {
        private Pagination pagination = new Pagination();

        public PaginationTestDataBuilder WithOffset(int value)
        {
            pagination.Offset = value;

            return this;
        }

        public PaginationTestDataBuilder WithRows(int value)
        {
            pagination.Rows = value;

            return this;
        }

        public Pagination Build()
        {
            return pagination;
        }

        public static PaginationTestDataBuilder Create()
        {
            return new PaginationTestDataBuilder();
        }
    }
}
