﻿using NSubstitute;
using Rates.Interfaces;
using Rates.Models;

namespace Rates.TestInfrastructure.Builders
{
    public class RatesRepositoryTestDataBuilder
    {
        private readonly IRatesRepository _ratesRepository;
        public RatesRepositoryTestDataBuilder()
        {
            _ratesRepository = Substitute.For<IRatesRepository>();
        }

        public RatesRepositoryTestDataBuilder WithGetRates(int offset, int row, IEnumerable<RatesDTO> rates)
        {
            _ratesRepository.GetRates(offset, row).Returns(rates);

            return this;
        }

        public RatesRepositoryTestDataBuilder WithSaveRates(out Captured<IEnumerable<RatesDTO>> captured)
        {
            var localCaptured = new Captured<IEnumerable<RatesDTO>>();
            captured = localCaptured;

            _ratesRepository.SaveRates(Arg.Do<IEnumerable<RatesDTO>>(rate => localCaptured.SetValue(rate)));

            return this;
        }

        public IRatesRepository Build()
        {
            return _ratesRepository;
        }

        public static RatesRepositoryTestDataBuilder Create()
        {
            return new RatesRepositoryTestDataBuilder();
        }
    }
}
