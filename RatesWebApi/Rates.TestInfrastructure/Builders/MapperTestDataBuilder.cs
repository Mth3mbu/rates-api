﻿using AutoMapper;
using NSubstitute;

namespace Rates.TestInfrastructure.Builders
{
    public class MapperTestDataBuilder
    {
        private readonly IMapper _mapper;
        public MapperTestDataBuilder()
        {
            _mapper = Substitute.For<IMapper>();
        }

        public IMapper Build()
        {
            return _mapper;
        }

        public static MapperTestDataBuilder Create()
        {
            return new MapperTestDataBuilder();
        }
    }
}
