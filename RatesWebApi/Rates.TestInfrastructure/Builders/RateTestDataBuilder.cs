﻿using Rates.Models;

namespace Rates.TestInfrastructure.Builders
{
    public class RateTestDataBuilder
    {
        private Rate rate = new Rate();

        public RateTestDataBuilder WithId(int id)
        {
            rate.Id = id;

            return this;
        }

        public RateTestDataBuilder WithName(string name)
        {
            rate.Name = name;

            return this;
        }

        public RateTestDataBuilder WithMarketPairs(int marketPairs)
        {
            rate.MarketPairs = marketPairs;

            return this;
        }

        public RateTestDataBuilder WithCmcRank(int rank)
        {
            rate.CmcRank = rank;

            return this;
        }

        public RateTestDataBuilder WithSymbol(string symbol)
        {
            rate.Symbol = symbol;

            return this;
        }

        public RateTestDataBuilder WithPlatform(string platform)
        {
            rate.Platform = platform;

            return this;
        }

        public RateTestDataBuilder WithSlug(string slug)
        {
            rate.Slug = slug;

            return this;
        }

        public RateTestDataBuilder WithTags(string[] tags)
        {
            rate.Tags = tags;

            return this;
        }

        public RateTestDataBuilder WithDateAdded(DateTime date)
        {
            rate.DateAdded = date;

            return this;
        }

        public RateTestDataBuilder WithLastUpdated(DateTime date)
        {
            rate.LastUpdated = date;

            return this;
        }

        public RateTestDataBuilder WithPrice(decimal value)
        {
            rate.Price = value;

            return this;
        }

        public RateTestDataBuilder WithMarketCap(decimal value)
        {
            rate.MarketCap = value;

            return this;
        }

        public RateTestDataBuilder WithMarketCapDominance(decimal value)
        {
            rate.MarketCapDominance = value;

            return this;
        }

        public RateTestDataBuilder WithFullyDilutedMarketcap(decimal value)
        {
            rate.FullyDilutedMarketcap = value;

            return this;
        }

        public RateTestDataBuilder WithVolume24h(decimal value)
        {
            rate.Volume24h = value;

            return this;
        }

        public RateTestDataBuilder WithVolumeChange24h(decimal value)
        {
            rate.VolumeChange24h = value;

            return this;
        }

        public RateTestDataBuilder WithPercentChange24h(decimal value)
        {
            rate.PercentChange24h = value;

            return this;
        }

        public RateTestDataBuilder WithPercentPercentChange7d(decimal value)
        {
            rate.PercentChange24h = value;

            return this;
        }

        public RateTestDataBuilder BuildRandom()
        {
            return this;
        }

        public RateTestDataBuilder WithRandomProps()
        {
            WithId(RandomPrimitives.RandomNumber())
                .WithCmcRank(RandomPrimitives.RandomNumber())
                .WithMarketPairs(RandomPrimitives.RandomNumber())
                .WithName(RandomPrimitives.RandomName())
                .WithSymbol(RandomPrimitives.RandomWord())
                .WithSlug(RandomPrimitives.RandomWord())
                .WithPlatform(RandomPrimitives.RandomWord())
                .WithTags(RandomPrimitives.RandomTags().ToArray())
                .WithDateAdded(RandomPrimitives.RandomDateBetween(DateTime.MinValue, DateTime.MaxValue))
                .WithLastUpdated(RandomPrimitives.RandomDateBetween(DateTime.Now, DateTime.MaxValue))
                .WithPrice(RandomPrimitives.RandomMoneyValue())
                .WithMarketCap(RandomPrimitives.RandomMoneyValue())
                .WithMarketCapDominance(RandomPrimitives.RandomMoneyValue())
                .WithFullyDilutedMarketcap(RandomPrimitives.RandomMoneyValue())
                .WithVolume24h(RandomPrimitives.RandomMoneyValue())
                .WithVolumeChange24h(RandomPrimitives.RandomMoneyValue())
                .WithPercentChange24h(RandomPrimitives.RandomMoneyValue())
                .WithPercentPercentChange7d(RandomPrimitives.RandomMoneyValue());

            return this;
        }

        public IEnumerable<Rate> BuildList(int count = 5)
        {
            var rateList = new List<Rate>();
            for (int i = 0; i < count; i++)
            {
                rateList.Add(Create().WithRandomProps().Build());
            }

            return rateList;
        }

        public Rate Build()
        {
            return rate;
        }

        public static RateTestDataBuilder Create()
        {
            return new RateTestDataBuilder();
        }
    }
}
