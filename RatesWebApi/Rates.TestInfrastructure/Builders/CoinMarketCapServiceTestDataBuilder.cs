﻿using NSubstitute;
using Rates.Interfaces;
using Rates.Models;

namespace Rates.TestInfrastructure.Builders
{
    public class CoinMarketCapServiceTestDataBuilder
    {
        private readonly ICoinMarketCapService _coinMarketCapService;
        public CoinMarketCapServiceTestDataBuilder()
        {
            _coinMarketCapService = Substitute.For<ICoinMarketCapService>();
        }

        public CoinMarketCapServiceTestDataBuilder WithGetRates(int offset, int rows, IEnumerable<Rate> rates)
        {
            _coinMarketCapService.GetRates(offset, rows).Returns(rates);

            return this;
        }

        public ICoinMarketCapService Build()
        {
            return _coinMarketCapService;
        }

        public static CoinMarketCapServiceTestDataBuilder Create()
        {
            return new CoinMarketCapServiceTestDataBuilder();
        }
    }
}
