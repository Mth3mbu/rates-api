﻿using Rates.Models;

namespace Rates.TestInfrastructure.Builders
{
    public class RatesDTOTestDataBuilder
    {
        private RatesDTO rate = new RatesDTO();

        public RatesDTOTestDataBuilder WithId(int id)
        {
            rate.Id = id;

            return this;
        }

        public RatesDTOTestDataBuilder WithName(string name)
        {
            rate.Name = name;

            return this;
        }

        public RatesDTOTestDataBuilder WithMarketPairs(int marketPairs)
        {
            rate.MarketPairs = marketPairs;

            return this;
        }

        public RatesDTOTestDataBuilder WithCmcRank(int rank)
        {
            rate.CmcRank = rank;

            return this;
        }

        public RatesDTOTestDataBuilder WithSymbol(string symbol)
        {
            rate.Symbol = symbol;

            return this;
        }

        public RatesDTOTestDataBuilder WithPlatform(string platform)
        {
            rate.Platform = platform;

            return this;
        }

        public RatesDTOTestDataBuilder WithSlug(string slug)
        {
            rate.Slug = slug;

            return this;
        }

        public RatesDTOTestDataBuilder WithTags(string[] tags)
        {
            rate.Tags = String.Join(",", tags);

            return this;
        }

        public RatesDTOTestDataBuilder WithDateAdded(DateTime date)
        {
            rate.DateAdded = date;

            return this;
        }

        public RatesDTOTestDataBuilder WithLastUpdated(DateTime date)
        {
            rate.LastUpdated = date;

            return this;
        }

        public RatesDTOTestDataBuilder WithPrice(decimal value)
        {
            rate.Price = value;

            return this;
        }

        public RatesDTOTestDataBuilder WithMarketCap(decimal value)
        {
            rate.MarketCap = value;

            return this;
        }

        public RatesDTOTestDataBuilder WithMarketCapDominance(decimal value)
        {
            rate.MarketCapDominance = value;

            return this;
        }

        public RatesDTOTestDataBuilder WithFullyDilutedMarketcap(decimal value)
        {
            rate.FullyDilutedMarketcap = value;

            return this;
        }

        public RatesDTOTestDataBuilder WithVolume24h(decimal value)
        {
            rate.Volume24h = value;

            return this;
        }

        public RatesDTOTestDataBuilder WithVolumeChange24h(decimal value)
        {
            rate.VolumeChange24h = value;

            return this;
        }

        public RatesDTOTestDataBuilder WithPercentChange24h(decimal value)
        {
            rate.PercentChange24h = value;

            return this;
        }

        public RatesDTOTestDataBuilder WithPercentPercentChange7d(decimal value)
        {
            rate.PercentChange24h = value;

            return this;
        }

        public RatesDTOTestDataBuilder BuildRandom()
        {
            return this;
        }

        public RatesDTOTestDataBuilder WithRandomProps()
        {
            WithId(RandomPrimitives.RandomNumber())
                .WithCmcRank(RandomPrimitives.RandomNumber())
                .WithMarketPairs(RandomPrimitives.RandomNumber())
                .WithName(RandomPrimitives.RandomName())
                .WithSymbol(RandomPrimitives.RandomWord())
                .WithSlug(RandomPrimitives.RandomWord())
                .WithPlatform(RandomPrimitives.RandomWord())
                .WithTags(RandomPrimitives.RandomTags().ToArray())
                .WithDateAdded(RandomPrimitives.RandomDateBetween(DateTime.Parse("1/1/1753"), DateTime.Now))
                .WithLastUpdated(RandomPrimitives.RandomDateBetween(DateTime.Parse("1/1/1953"), DateTime.Now))
                .WithPrice(RandomPrimitives.RandomMoneyValue())
                .WithMarketCap(RandomPrimitives.RandomMoneyValue())
                .WithMarketCapDominance(RandomPrimitives.RandomMoneyValue())
                .WithFullyDilutedMarketcap(RandomPrimitives.RandomMoneyValue())
                .WithVolume24h(RandomPrimitives.RandomMoneyValue())
                .WithVolumeChange24h(RandomPrimitives.RandomMoneyValue())
                .WithPercentChange24h(RandomPrimitives.RandomMoneyValue())
                .WithPercentPercentChange7d(RandomPrimitives.RandomMoneyValue());

            return this;
        }

        public IEnumerable<RatesDTO> BuildList(int count = 5)
        {
            var rateList = new List<RatesDTO>();
            for (int i = 0; i < count; i++)
            {
                rateList.Add(Create().WithRandomProps().Build());
            }

            return rateList;
        }

        public RatesDTO Build()
        {
            return rate;
        }

        public static RatesDTOTestDataBuilder Create()
        {
            return new RatesDTOTestDataBuilder();
        }
    }
}
