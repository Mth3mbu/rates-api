﻿namespace Rates.TestInfrastructure
{
    public class Pagination
    {
        public int Offset { get; set; }
        public int Rows { get; set; }
    }
}
