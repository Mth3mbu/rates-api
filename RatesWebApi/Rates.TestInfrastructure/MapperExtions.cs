﻿using Rates.Models;

namespace Rates.TestInfrastructure
{
    public static class MapperExtions
    {
        public static IEnumerable<RatesDTO> ToRatesDto(this IEnumerable<Rate> rates)
        {

            return rates.Select(rate => new RatesDTO
            {
                Id = rate.Id,
                Name = rate.Name,
                MarketCap = rate.MarketCap,
                CmcRank = rate.CmcRank,
                Symbol = rate.Symbol,
                Platform = rate.Platform,
                Slug = rate.Slug,
                Tags = string.Join(",", rate.Tags),
                DateAdded = rate.DateAdded,
                LastUpdated = rate.LastUpdated,
                Price = rate.Price,
                MarketCapDominance = rate.MarketCapDominance,
                FullyDilutedMarketcap = rate.FullyDilutedMarketcap,
                Volume24h = rate.Volume24h,
                VolumeChange24h = rate.VolumeChange24h,
                PercentChange24h = rate.PercentChange24h,
                PercentChange7d = rate.PercentChange7d,
                MarketPairs = rate.MarketPairs,
            });
        }
    }
}
