﻿using AutoMapper;
using Rates.CoinMarketcap.Intergration.Models;
using Rates.Interfaces;
using Rates.Models;
using RestSharp;

namespace Rates.CoinMarketcap.Intergration
{
    public class CoinMarketCapService : ICoinMarketCapService
    {
        private readonly ICoinMarketCapSettings _coinMarketCapSettings;
        private readonly IMapper _mapper;
        public CoinMarketCapService(ICoinMarketCapSettings coinMarketCapSettings, IMapper mapper)
        {
            _coinMarketCapSettings = coinMarketCapSettings;
            _mapper = mapper;
        }

        public async Task<IEnumerable<Rate>> GetRates(int start = 1, int limit = 10)
        {
            var query = $"cryptocurrency/listings/latest?start={start}&limit={limit}";
            var results = await SendRequest<Response>(query);

            return results?.Data?.data.Select(response => _mapper.Map<Rate>(response)); ;
        }

        private async Task<RestResponse<T>> SendRequest<T>(string query)
        {
            var url = $"{_coinMarketCapSettings.BaseUrl}/{query}";
            var client = new RestClient(url);
            var request = new RestRequest();

            request.AddHeader("X-CMC_PRO_API_KEY", _coinMarketCapSettings.ApiKey);
            request.AddHeader("Accept", " application/json");

            return await client.ExecuteAsync<T>(request);
        }
    }
}
