﻿using AutoMapper;
using Nancy.Json;
using Rates.CoinMarketcap.Intergration.Models;
using Rates.Models;

namespace Rates.CoinMarketcap.Intergration.Mapper
{
    public class CoinMarketcapRateProfile : Profile
    {
        public CoinMarketcapRateProfile()
        {
            CreateMap<CoinMarketCapRate, Rate>()
                .ForMember(dest => dest.Price, prop => prop.MapFrom(src => src.quote.USD.price))
                .ForMember(dest => dest.Volume24h, prop => prop.MapFrom(src => src.quote.USD.volume_24h))
                .ForMember(dest => dest.MarketCap, prop => prop.MapFrom(src => src.quote.USD.market_cap))
                .ForMember(dest => dest.FullyDilutedMarketcap, prop => prop.MapFrom(src => src.quote.USD.fully_diluted_market_cap))
                .ForMember(dest => dest.PercentChange24h, prop => prop.MapFrom(src => src.quote.USD.percent_change_24h))
                .ForMember(dest => dest.PercentChange7d, prop => prop.MapFrom(src => src.quote.USD.percent_change_7d))
                .ForMember(dest => dest.LastUpdated, prop => prop.MapFrom(src => DateTime.Parse(src.last_updated)))
                .ForMember(dest => dest.DateAdded, prop => prop.MapFrom(src => DateTime.Parse(src.date_added)))
                .ForMember(dest => dest.Tags, prop => prop.MapFrom(src => src.tags ?? null))
                .ForMember(dest => dest.MarketCapDominance, prop => prop.MapFrom(src => src.quote.USD.market_cap_dominance))
                .ForMember(dest => dest.Platform, prop => prop.MapFrom(src => new JavaScriptSerializer().Serialize(src.platform)))
                .ForMember(dest => dest.Symbol, prop => prop.MapFrom(src => src.symbol.ToString()))
                .ForMember(dest => dest.CmcRank, prop => prop.MapFrom(src => src.cmc_rank))
                .ForMember(dest => dest.MarketPairs, prop => prop.MapFrom(src => src.num_market_pairs))
                .ForMember(dest => dest.Slug, prop => prop.MapFrom(src => src.slug))
                .ForMember(dest => dest.Id, prop => prop.MapFrom(src => src.id))
                .ForMember(dest => dest.Name, prop => prop.MapFrom(src => src.name));
        }
    }
}
