﻿using AutoMapper;
using Rates.Models;

namespace Rates.CoinMarketcap.Intergration.Mapper
{
    public class RateProfile : Profile
    {
        public RateProfile()
        {
            CreateMap<Rate, RatesDTO>().ForMember(dest => dest.Tags, prop => prop.MapFrom(src => string.Join(',', src.Tags)));
        }
    }
}
