﻿using AutoMapper;
using Rates.Models;

namespace Rates.CoinMarketcap.Intergration.Mapper
{
    public class RatesDTOProfile : Profile
    {
        public RatesDTOProfile()
        {
            CreateMap<RatesDTO, Rate>().ForMember(dest => dest.Tags, prop => prop.MapFrom(src => src.Tags.Split(",", StringSplitOptions.None).ToArray()));
        }
    }
}
