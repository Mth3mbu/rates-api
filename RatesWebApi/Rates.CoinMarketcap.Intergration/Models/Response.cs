﻿namespace Rates.CoinMarketcap.Intergration.Models
{
    internal class Response
    {
        public CoinMarketCapRate[] data { get; set; }
        public Status status { get; set; }  
    }
}
