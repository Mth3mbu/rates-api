﻿namespace Rates.CoinMarketcap.Intergration.Models
{
    internal class Status
    {
        public string timestamp { get; set; }
        public int elapsed { get; set; }
        public int credit_count { get; set; }
        public int total_count { get; set; }
        public int error_code { get; set; }
        public string error_message { get; set; }
        public string notice { get; set; }
    }
}
