﻿namespace Rates.CoinMarketcap.Intergration.Models
{
    internal class CoinMarketCapRate
    {
        public int id { get; set; }
        public string name { get; set; }
        public string symbol { get; set; }
        public string slug { get; set; }
        public int num_market_pairs { get; set; }
        public int cmc_rank { get; set; }
        public string date_added { get; set; }
        public string[] tags { get; set; }
        public Platform platform { get; set; }
        public string last_updated { get; set; }
        public object max_supply { get; set; }
        public object circulating_supply { get; set; }
        public object total_supply { get; set; } 
        public object self_reported_circulating_supply { get; set; }  
        public object self_reported_market_cap { get; set; }
        public Quote quote { get; set; }
    }
}
