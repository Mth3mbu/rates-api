﻿using NSubstitute;
using Rates.Interfaces.connection;
using Rates.TestInfrastructure;
using System.Data;

namespace Rates.Repositories.Tests.connection
{
    public class RatesDbConnectionContextTestDataBuilder
    {
        private readonly IRatesDbConnectionContext _dbConnectionContext;
        public RatesDbConnectionContextTestDataBuilder()
        {
            _dbConnectionContext = Substitute.For<IRatesDbConnectionContext>();
        }

        public RatesDbConnectionContextTestDataBuilder WithConnection(IDbConnection connection)
        {
            _dbConnectionContext.GetConnection().Returns(connection);

            return this;
        }

        public RatesDbConnectionContextTestDataBuilder WithTransaction(IDbTransaction transaction)
        {
            _dbConnectionContext.GetTransaction().Returns(transaction);

            return this;
        }

        public RatesDbConnectionContextTestDataBuilder CapturingRollbacks(out Captured<bool> captured)
        {
            var localCaptured = new Captured<bool>();
            captured = localCaptured;

            _dbConnectionContext.When(context => context.Rollback()).Do(info => localCaptured.SetValue(true));

            return this;
        }

        public IRatesDbConnectionContext Build()
        {
            return _dbConnectionContext;
        }

        public static RatesDbConnectionContextTestDataBuilder Create()
        {

            return new RatesDbConnectionContextTestDataBuilder();
        }

    }
}
