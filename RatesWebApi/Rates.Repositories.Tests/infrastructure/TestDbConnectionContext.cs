﻿using System.Data;

namespace Rates.Repositories.Tests.infrastructure
{
    public class TestDbConnectionContext : IDisposable, ITestDbConnectionContext
    {
        public IDbConnection Connection { get; set; }
        public IDbTransaction Transaction { get; set; }

        public void Dispose()
        {
            Transaction?.Dispose();
            Connection?.Dispose();
        }
    }
}
