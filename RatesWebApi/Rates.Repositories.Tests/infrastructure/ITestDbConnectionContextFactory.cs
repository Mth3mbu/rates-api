﻿namespace Rates.Repositories.Tests.infrastructure
{
    public interface ITestDbConnectionContextFactory
    {
        string ConnectionString { get; }
        TestDbConnectionContext Create();
    }
}
