﻿using NUnit.Framework;

namespace Rates.Repositories.Tests.infrastructure
{
    public class DbTestBase
    {
        protected ITestDbConnectionContextFactory TestDbConnectionContextFactory => SetupTestDb.TestDbConnectionContextFactory;

        [SetUp]
        public void SetUp()
        {

        }

        [TearDown]
        public void TearDown()
        {
        }
    }
}
