﻿using System.Data;

namespace Rates.Repositories.Tests.infrastructure
{
    public interface ITestDbConnectionContext
    {
        IDbConnection Connection { get; set; }
        IDbTransaction Transaction { get; set; }
    }
}
