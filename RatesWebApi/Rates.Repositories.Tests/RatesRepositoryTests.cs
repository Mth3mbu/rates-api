﻿using Dapper;
using FluentAssertions;
using NUnit.Framework;
using Rates.Interfaces;
using Rates.Repositories.Tests.connection;
using Rates.Repositories.Tests.infrastructure;
using Rates.TestInfrastructure;
using Rates.TestInfrastructure.Builders;

namespace Rates.Repositories.Tests
{
    [TestFixture(Category = TestCategories.Integration)]
    public class RatesRepositoryTests
    {
        public static ITestDbConnectionContextFactory TestDbConnectionContextFactory;

        [OneTimeSetUp]
        public void Init()
        {
            var _setup = new SetupLocalDbForTesting(Guid.NewGuid().ToString(), "TestDb");
            TestDbConnectionContextFactory = _setup.TestDbConnectionContextFactory;
        }

        [Test]
        public async Task GivenRates_WhenAdding_ShouldSaveToDb()
        {
            // Arrange
            var pagination = PaginationTestDataBuilder.Create()
                .WithOffset(0)
                .WithRows(5)
                .Build();
            var rates = RatesDTOTestDataBuilder.Create().BuildList().OrderBy(x => x.Id);
            using var context = TestDbConnectionContextFactory.Create();
            var sut = CreareRepository(context);
            // Act
            await ClearRatesTable(context);
            await sut.SaveRates(rates);
            // Assert
            var expected = await sut.GetRates(pagination.Offset, pagination.Rows);
            expected.Should().NotBeNullOrEmpty();
            expected.Count().Should().Be(rates.Count());
            expected.ToJson().Should().Equals(rates.ToJson());  
        }

        [Test]
        public async Task GivenPagination_WithinDataRange_ShouldReturnRates()
        {
            // Arrange
            var pagination = PaginationTestDataBuilder.Create()
                .WithOffset(0)
                .WithRows(5)
                .Build();
            var rates = RatesDTOTestDataBuilder.Create().BuildList();
            using var context = TestDbConnectionContextFactory.Create();
            var sut = CreareRepository(context);
            // Act
            await ClearRatesTable(context);
            await sut.SaveRates(rates);
            // Assert
            var expected = await sut.GetRates(pagination.Offset, pagination.Rows);
            expected.Should().NotBeNullOrEmpty();
            expected.Count().Should().Be(rates.Count());
            expected.ToJson().Should().Equals(rates.ToJson());
        }

        [Test]
        public async Task GivenPagination_OutSideDataRange_ShouldReturnEmptyList()
        {
            // Arrange
            var pagination = PaginationTestDataBuilder.Create()
                .WithOffset(6)
                .WithRows(5)
                .Build();
            var rates = RatesDTOTestDataBuilder.Create().BuildList();
            using var context = TestDbConnectionContextFactory.Create();
            var sut = CreareRepository(context);
            // Act
            await ClearRatesTable(context);
            await sut.SaveRates(rates);
            // Assert
            var expected = await sut.GetRates(pagination.Offset, pagination.Rows);
            expected.Should().BeNullOrEmpty();
        }

        public IRatesRepository CreareRepository(TestDbConnectionContext context)
        {
            var dbConnectionFactory = RatesDbConnectionContextTestDataBuilder.Create()
              .WithConnection(context.Connection)
              .WithTransaction(context.Transaction)
              .Build();

            return new RatesRepository(dbConnectionFactory);
        }

        private static async Task ClearRatesTable(TestDbConnectionContext context)
        {
            await context.Connection.ExecuteAsync("DELETE FROM [Rates]", transaction: context.Transaction);
        }
    }
}
