using FluentMigrator.Runner;
using Rates.CoinMarketcap.Intergration.Mapper;
using Rates.Repositories.Migrations;
using RatesWebApi.Infrastructure;
using RatesWebApi.IoCConfig;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services
    .AddSwaggerGen()
    .AddServices()
    .AddRepositories()
    .AddMigrations()
    .AddSettings();

builder.Services.AddAutoMapper(typeof(IMapperProfiles));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

SetCors(app, app.Configuration);

RunDbMigrator(app);

app.Run();

void SetCors(IApplicationBuilder app, IConfiguration configuration)
{
    var corsSettings = configuration.Read<CorsSettings>("Cors");
    app.UseCors(builder => builder
        .WithOrigins(corsSettings.Origins)
        .AllowAnyHeader()
        .AllowAnyMethod()
    );
}

void RunDbMigrator(IApplicationBuilder app)
{
    var conn = builder.Configuration.GetConnectionString("MasterDb");
    Database.EnsureDatabase(conn, "RatesDb");

    using var startupScope = app.ApplicationServices.CreateScope();
    var migrationRunner = startupScope.ServiceProvider.GetService<IMigrationRunner>();
    migrationRunner?.MigrateUp();
}
