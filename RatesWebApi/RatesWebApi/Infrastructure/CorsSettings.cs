﻿namespace RatesWebApi.Infrastructure
{
    public class CorsSettings
    {
        public string[] Origins { get; set; }
    }
}
