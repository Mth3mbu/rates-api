﻿using Rates.Interfaces;

namespace RatesWebApi.Infrastructure
{
    public class CoinMarketCapSettings : ICoinMarketCapSettings
    {
        public CoinMarketCapSettings(IConfiguration configuration)
        {
            configuration.ReadOnto("Coinmarketcap", this);
        }
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
    }
}
