﻿

using Rates.Interfaces;

namespace RatesWebApi.Infrastructure
{
    public class DbSettings : IDbSettings
    {
        private readonly IConfiguration _configuration;
        public DbSettings(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string ConnectionString => _configuration.GetConnectionString("RatesDb");
    }
}
