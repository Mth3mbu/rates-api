﻿using Rates.Interfaces;
using Rates.Interfaces.connection;
using Rates.Repositories;
using Rates.Repositories.Connections;
using RatesWebApi.Infrastructure;

namespace RatesWebApi.IoCConfig
{
    public static class RepositoryIoCExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IDbSettings, DbSettings>();
            services.AddScoped<IRatesDbConnectionContext>(
                provider => new AutoConnectingDbConnectionContext(provider.GetService<IDbSettings>(), settings => settings.ConnectionString));

            services.AddScoped<IRatesRepository, RatesRepository>();


            return services;
        }
    }
}
