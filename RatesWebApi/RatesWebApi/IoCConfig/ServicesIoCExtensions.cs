﻿using Rates.CoinMarketcap.Intergration;
using Rates.Interfaces;
using Rates.Services;

namespace RatesWebApi.IoCConfig
{
    public static class ServicesIoCExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<ICoinMarketCapService, CoinMarketCapService>();
            services.AddScoped<IRatesService, RatesService>();

            return services;
        }
    }
}
