﻿using Rates.Interfaces;
using RatesWebApi.Infrastructure;

namespace RatesWebApi.IoCConfig
{
    public static class SettingIoCExtensions
    {
        public static IServiceCollection AddSettings(this IServiceCollection services)
        {
            services.AddScoped<ICoinMarketCapSettings, CoinMarketCapSettings>();

            return services;
        }
    }
}
