﻿using Microsoft.AspNetCore.Mvc;
using Rates.Interfaces;

namespace RatesWebApi.Controllers
{
    [ApiController]
    [Route("rates")]
    public class RatesController : ControllerBase
    {
        private readonly IRatesService _ratesService;
        public RatesController(IRatesService ratesService)
        {
            _ratesService = ratesService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int start = 1, int limit = 10)
        {
            var res = await _ratesService.GetRates(start, limit);

            return Ok(res);
        }
    }
}
