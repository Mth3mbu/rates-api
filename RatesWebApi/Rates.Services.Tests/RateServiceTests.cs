using DeepEqual.Syntax;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using Rates.TestInfrastructure;
using Rates.TestInfrastructure.Builders;
using System.Linq;
using System.Threading.Tasks;

namespace Rates.Services.Tests
{
    [TestFixture]
    public class RateServiceTests
    {
        [Test]
        public async Task GivenPagination_AboveLocalDatabaseRecordsRange_ShouldReturnRatesFromCoinMarketCap()
        {
            //Arrange
            var pagination = PaginationTestDataBuilder.Create()
                .WithOffset(1)
                .WithRows(10)
                .Build();
            var rates = RateTestDataBuilder.Create()
                .BuildList(10)
                .ToList();
            var ratesRepository = RatesRepositoryTestDataBuilder.Create().Build();
            var coinMarketCapService = CoinMarketCapServiceTestDataBuilder.Create()
                .WithGetRates(pagination.Offset, pagination.Rows, rates)
                .Build();
            var mapper = MapperTestDataBuilder.Create().Build();
            var sut = new RatesService(coinMarketCapService, ratesRepository, mapper);
            //Act
            var response = await sut.GetRates(pagination.Offset, pagination.Rows);
            //Assert
            response.Should().NotBeNullOrEmpty();
            response.ToList().Count.Should().Be(rates.Count);
            response.ShouldDeepEqual(rates);
            await ratesRepository.Received().GetRates(pagination.Offset - 1, pagination.Rows);
            await coinMarketCapService.Received().GetRates(pagination.Offset, pagination.Rows);
        }

        [Test]
        public async Task GivenPagination_AboveLocalDatabaseRecordsRange_ShouldReturnRatesFromCoinMarketCap_AndSaveToLocalDataBase()
        {
            //Arrange
            var pagination = PaginationTestDataBuilder.Create()
                .WithOffset(1)
                .WithRows(10)
                .Build();
            var rates = RateTestDataBuilder.Create().BuildList(10).ToList();
            var coinMarketCapService = CoinMarketCapServiceTestDataBuilder.Create()
                .WithGetRates(pagination.Offset, pagination.Rows, rates)
                .Build();
            var ratesRepository = RatesRepositoryTestDataBuilder.Create()
                .WithSaveRates(out var captured)
                .Build();
            var mapper = MapperTestDataBuilder.Create().Build();
            var sut = new RatesService(coinMarketCapService, ratesRepository, mapper);
            //Act
            var response = await sut.GetRates(pagination.Offset, pagination.Rows);
            //Assert
            response.Should().NotBeNullOrEmpty();
            captured.Value.Should().NotBeNull();
            captured.Value.Count().Should().Be(rates.Count);
        }

        [Test]
        public async Task GivenPagination_WithinLocalDatabaseRecordsRange_ShouldReturnLocalRates()
        {
            //Arrange
            var pagination = PaginationTestDataBuilder.Create()
                .WithOffset(1)
                .WithRows(10)
                .Build();
            var rates = RatesDTOTestDataBuilder.Create()
                .BuildList(10)
                .ToArray();
            var ratesRepository = RatesRepositoryTestDataBuilder.Create()
                .WithGetRates(pagination.Offset - 1, pagination.Rows, rates)
                .Build();
            var coinMarketCapService = CoinMarketCapServiceTestDataBuilder.Create().Build();
            var mapper = MapperTestDataBuilder.Create().Build();
            var sut = new RatesService(coinMarketCapService, ratesRepository, mapper);
            //Act
            var response = await sut.GetRates(pagination.Offset, pagination.Rows);
            //Assert
            response.Should().NotBeNullOrEmpty();
            response.ToList().Count.Should().Be(rates.Length);
            await coinMarketCapService.DidNotReceive().GetRates(pagination.Offset - 1, pagination.Rows);
        }
    }
}