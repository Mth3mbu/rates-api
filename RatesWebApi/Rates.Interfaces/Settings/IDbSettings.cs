﻿namespace Rates.Interfaces
{
    public interface IDbSettings
    {
        public string ConnectionString { get; }
    }
}
