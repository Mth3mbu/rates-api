﻿namespace Rates.Interfaces
{
    public interface ICoinMarketCapSettings
    {
        public string ApiKey { get; set; }
        public string BaseUrl { get; set; }
    }
}
