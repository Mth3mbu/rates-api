﻿using Rates.Models;

namespace Rates.Interfaces
{
    public interface IRatesRepository
    {
        Task<IEnumerable<RatesDTO>> GetRates(int start, int limit);
        Task SaveRates(IEnumerable<RatesDTO> rates);
    }
}
