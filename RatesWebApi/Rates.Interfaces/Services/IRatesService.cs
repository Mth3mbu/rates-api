﻿using Rates.Models;

namespace Rates.Interfaces
{
    public interface IRatesService
    {
        Task<IEnumerable<Rate>> GetRates(int start = 1, int limit = 10);
    }
}
