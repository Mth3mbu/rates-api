﻿using Rates.Models;

namespace Rates.Interfaces
{
    public interface ICoinMarketCapService
    {
        Task<IEnumerable<Rate>> GetRates(int start = 1, int limit = 10);
    }
}
